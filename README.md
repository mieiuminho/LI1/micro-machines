[trello]: https://trello.com/b/pQNROiMB
[haskell-platform]: https://www.haskell.org/platform/

# Micro Machines

## Setup

Make sure you have `haskell-platform` installed. You can read more
[here][haskell-platform]. After that, you need to install all the dependencies
of the project. Just run `cabal install --only-depencies` once.

### Build

In order to build, just run `cabal build`. Play by running `cabal run`.

### Documentation

You can generate the documentation by running `cabal haddock --executables`.

### Testing

To run the tests type `cabal test`. If you are having problems, you can run 
`cabal configure --enable-tests` before.

#### Coverage

To see the test-suite coverage of all modules run the following:

```shell
cabal clean
cabal configure --enable-tests --enable-coverage
cabal test
```

Don't forget to install HPC by running `cabal install hpc`.

## Team

![Nelson Estevão][nelson-pic] | ![Pedro Ribeiro][pedro-pic] | ![Rui Mendes][rui-pic]
:---: | :---: | :---:
[Nelson Estevão][nelson] | [Pedro Ribeiro][pedro] | [Rui Mendes][rui]

[nelson]: https://github.com/nelsonmestevao
[nelson-pic]: https://github.com/nelsonmestevao.png?size=120
[pedro]: https://github.com/pedroribeiro22
[pedro-pic]: https://github.com/pedroribeiro22.png?size=120
[rui]: https://github.com/ruimendes29
[rui-pic]: https://github.com/ruimendes29.png?size=120
