{-|
Module      : Graphics.Game
Description : Game Graphics Configuration
Copyright   : Nelson Estevão <hello@estevao.xyz>;
            : Pedro Ribeiro  <>;
            : Rui Mendes <>;

//TODO: Add module description
-}

module Graphics.Game where

import Data.Game
import Data.Helper

import Graphics.Gloss                      -- interface gloss
import Graphics.Gloss.Data.Picture         -- importar o tipo Picture
import Graphics.Gloss.Interface.Pure.Game  -- importar o tipo Event

-- | Número de frames por segundo.
frameRate :: Int
frameRate = 60

-- | A cor do background.
bgColor :: Color
bgColor = white

-- | Dimensões e título da janela do jogo.
window :: Display
window = InWindow
            "Micro Machines" -- título da janela
            (1000,1000)      -- dimensão da janela
            (0,0)            -- posição no ecrã

