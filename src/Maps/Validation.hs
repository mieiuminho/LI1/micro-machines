{-|
Module      : Maps.Validation
Description : Verify is a Map is valid
Copyright   : Nelson Estevão <hello@estevao.xyz>;
            : Pedro Ribeiro  <>;
            : Rui Mendes <>;

//TODO: Add module description
-}

module Maps.Validation where

import Data.Game
import Data.Helper
import Data.List

{- Para verificar que o Mapa é Rectangular. Só substitui o x e o y por pl(Primeira Linha) e sl(Segunda Linha) e pus
rl (Resto das Linhas) para ficar mais legivel e simplifiquei o caso da lista
de dois elementos -}
checkRectangulo :: Tabuleiro -> Bool
checkRectangulo [x] = True
checkRectangulo [pl,sl] = length pl == length sl
checkRectangulo (pl:sl:rl) = (length pl == length sl) && checkRectangulo (sl:rl)

--Auxiliares
fullLava :: [Peca] -> Bool
fullLava = foldr (\ h -> (&&) (h == Peca Lava 0)) True

firstAndLastLava :: [Peca] -> Bool
firstAndLastLava l = head l == Peca Lava 0 && last l == Peca Lava 0

-- | Para verificar as bordas de Lava
bordersLava :: Tabuleiro-> [Int] -> Bool
bordersLava [x] _ = fullLava x --Para garantir que testa a ultima linha ser toda lava
bordersLava (x:xs) (y:ys) | y == 0 = fullLava x && bordersLava xs ys --Para a primeira linha
                          | otherwise = firstAndLastLava x && bordersLava xs ys --Para o resto das linhas

-- | Para ver se a orientação inicial e a peça inicial se encaixam --
oriInitpossivel :: Orientacao -> Peca -> Bool
oriInitpossivel o (Peca tipo alt) = case tipo of
                                    (Curva oc) -> oc == o || oc == turnRight o
                                    (Rampa or) -> or == o || or == turnAround o
                                    Lava       -> False
                                    Recta      -> True

-- | Para ver se as alturas de duas peças são compativeis
eProxPAlt::Peca->Orientacao->Peca->Bool
eProxPAlt (Peca tipoPeca0 alt0) o (Peca tipoPeca1 alt1) = case tipoPeca0 of
                                                            (Rampa or) -> if or == o
                                                                          then case tipoPeca1 of
                                                                              Rampa oe -> if oe == o
                                                                                          then alt1 == 1+alt0
                                                                                          else alt0 == alt1
                                                                              _ -> alt1 == alt0+1
                                                                          else case tipoPeca1 of
                                                                              Rampa oe -> if oe == o
                                                                                          then alt0 == alt1
                                                                                          else alt1 == alt0-1
                                                                              _ -> alt0 == alt1
                                                            _ -> case tipoPeca1 of
                                                                 Rampa or -> if or == turnAround o
                                                                              then alt0-1 == alt1
                                                                              else alt0 == alt1
                                                                 _        -> alt0 == alt1

-- | Para ver se as entradas de duas peças são compativeis
eProxPOri::Orientacao->Tipo->Bool
eProxPOri o tipo_peca1 = ((tipo_peca1==(Rampa o))||
                         (tipo_peca1==(Rampa (turnAround o)))||
                         (tipo_peca1==(Curva o))||
                         (tipo_peca1==(Curva (turnRight o)))||
                         (tipo_peca1==Recta))

proxOri :: Orientacao -> Peca -> Orientacao
proxOri o (Peca tipo _) = case tipo of
                          Recta      -> o
                          (Rampa _)  -> o
                          (Curva or) -> if or == o
                                        then turnRight o
                                        else turnLeft o


-- | Dá a lista das posiçoes das peças válidas do tabuleiro até voltar à peça inicial
guardaPosVal::Mapa->(Peca,Posicao)->Orientacao->[Posicao]
guardaPosVal m@(Mapa (posi,oi) t) (p,pos) pO| novoPar==(getPecaFromTabuleiro posi t,posi)&&(eProxPOri oS tpP)&&(eProxPAlt p oS pP)&&oS==oi = [pos,posi]
                                            | (eProxPOri oS tpP)&&(eProxPAlt p oS pP) = pos:guardaPosVal m novoPar oS
                                            | otherwise = []
                                            where oS = proxOri pO p
                                                  pP = getPecaFromTabuleiro novaPos t
                                                  tpP = getTipoFromPeca pP
                                                  novoPar = (pP,novaPos)
                                                  novaPos = move pos oS

-- | Dá a lista de todas as posições possiveis no tabuleiro
allPos::(Int,Int)->[(Int,Int)]
allPos (a,b) = [(x,y)| x <- [0..(a-1)] , y <- [0..(b-1)]]

-- | Verifica se todas as peças fora da trajetória válida são Lava
verificaTudoLava::Tabuleiro->[Posicao]->[Posicao]->Bool
verificaTudoLava t [] vpos = True
verificaTudoLava t (hapos:tapos) vpos | hapos `notElem` vpos = (getPecaFromTabuleiro hapos t) == (Peca Lava 0)&&verificaTudoLava t tapos vpos
                                      | otherwise = True && verificaTudoLava t tapos vpos

-- | Dado um tabuleiro dá a sua dimensão
dimensaoTabuleiro::Tabuleiro->(Int,Int)
dimensaoTabuleiro (l:c) = (tx,ty)
                                 where tx = length l
                                       ty = length (l:c)

-- | Função que verifica se um mapa é válido
valida :: Mapa -> Bool
valida m@(Mapa (posi,oi) t) = oriInitpossivel oi (getPecaFromTabuleiro posi t) &&
                              checkRectangulo t &&
                              bordersLava t [0..(ty-1)] &&
                              verificaTudoLava t (allPos (tx,ty)) posValidas
                              && head posValidas == last posValidas
                              where (tx,ty) = dimensaoTabuleiro t
                                    posValidas = guardaPosVal m (getPecaFromTabuleiro posi t,posi) oi
