{-|
Module      : Main
Description : Game implementation
Copyright   : Nelson Estevão <hello@estevao.xyz>;
            : Pedro Ribeiro  <>;
            : Rui Mendes <>;

//TODO: Add module description
-}

module Main where

import Data.Game
import Data.Helper
import Maps.Build
import Maps.Validation
import Engine.Atualiza
import Engine.Movimenta
import Engine.Refresh
import Graphics.Game
import Graphics.Pictures
import Bots.Easy

import Graphics.Gloss                      -- interface gloss
import Graphics.Gloss.Data.Picture         -- importar o tipo Picture
import Graphics.Gloss.Interface.Pure.Game  -- importar o tipo Event

-- main :: IO ()
-- main = play window
--             bgColor
--             frameRate
main = undefined

-- play    :: Display              -- ^ Display mode.
--         -> Color                -- ^ Background color.
--         -> Int                  -- ^ Number of simulation steps to take for each second of real time.
--         -> world                -- ^ The initial world.
--         -> (world -> Picture)   -- ^ A function to convert the world a picture.
--         -> (Event -> world -> world)
--                 -- ^ A function to handle input events.
--         -> (Float -> world -> world)
--                 -- ^ A function to step the world one iteration.
--                 --   It is passed the period of time (in seconds) needing to be advanced.
--         -> IO ()
