{-|
Module      : Data.Helper
Description : Helper to manipulate the Data
Copyright   : Nelson Estevão <hello@estevao.xyz>;
            : Pedro Ribeiro  <>;
            : Rui Mendes <>;

//TODO: Add module description
-}

module Data.Helper where

import Data.Game

-- | A função @mapPair@ é usada para aplicar a mesma função a ambos os elementos de um par.
mapPair :: (a -> b) -> (a, a) -> (b, b)
mapPair f (a1, a2) = (f a1, f a2)

-- | A função @sumPair@ é usada para dois pares.
sumPair :: Num a => (a,a) -> (a,a) -> (a,a)
sumPair (a,b) (c,d) = (a+c,b+d)

-- | Função que dá o __ou__ exclusivo.
xor :: Bool -> Bool -> Bool
xor a b = a /= b

-- | A função @acontecimento@ é usada para converter um booleano em binário.
acontecimento :: Bool -> Double
acontecimento b = if b then 1 else 0

-- | A função move recebe uma posição e dada uma certa orientação altera a posição inicial
move :: Posicao -> Orientacao -> Posicao
move (x,y) o = case o of
               Norte -> (x,y-1)
               Sul   -> (x,y+1)
               Este  -> (x+1,y)
               Oeste -> (x-1,y)

-- | A função oriCon define qual a orientação contrária à orientação dada.
turnAround :: Orientacao -> Orientacao
turnAround o = case o of
                Norte -> Sul
                Sul   -> Norte
                Este  -> Oeste
                Oeste -> Este

-- | A função oriDir define qual a orientação resultante quando uma certa orientação roda à direita.
turnRight :: Orientacao->Orientacao
turnRight o = case o of
              Norte -> Este
              Sul   -> Oeste
              Este  -> Sul
              Oeste -> Norte

-- | A função oriEsq define qual a orientação resultante quando uma certa orientação roda à esquerda.
turnLeft :: Orientacao -> Orientacao
turnLeft o = case o of
             Norte -> Oeste
             Sul   -> Este
             Este  -> Norte
             Oeste -> Sul

-- | Função que devolve só a altura de uma peça.
getAlturaFromPeca :: Peca -> Int
getAlturaFromPeca (Peca _ a) = a

-- | Função que devolve só o tipo de uma peça.
getTipoFromPeca :: Peca -> Tipo
getTipoFromPeca (Peca t _) = t

-- | Função que devolve só a posição de um carro.
getPosicaoFromCarro :: Carro -> Posicao
getPosicaoFromCarro (Carro pt _ _) = mapPair floor pt

-- | Função que recebe um índice e devolve a peça que está nessa posição no tabuleiro.
getPecaFromTabuleiro :: Posicao -> Tabuleiro -> Peca
getPecaFromTabuleiro (x,y) tb = tb !! y !! x

-- | Função que recebe um Mapa e devolve um Tabuleiro.
getTabuleiroFromMap :: Mapa -> Tabuleiro
getTabuleiroFromMap (Mapa _ tb) = tb

-- | Função que recebe um Mapa e um Carro e devolve a peça em que este se encontra.
getPecaFromCarro :: Mapa -> Carro -> Peca
getPecaFromCarro (Mapa _ tb) c = getPecaFromTabuleiro p tb
                                    where p = getPosicaoFromCarro c

-- | Função que recebe um Jogo, o identificador do jogador e devolve o Carro correspondente.
getCarroFromJogo :: Jogo -> Int -> Carro
getCarroFromJogo (Jogo _ _ cs _ _) i = cs !! i

-- | Função que recebe um Jogo e devolve um Mapa.
getMapaFromJogo :: Jogo -> Mapa
getMapaFromJogo (Jogo m _ _ _ _) = m

-- | Função que devolve só as propriedades de um jogo.
getPropriedadesFromJogo :: Jogo -> Propriedades
getPropriedadesFromJogo (Jogo _ p _ _ _) = p

-- | Função que devolve só a lista de carros de um Jogo.
getCarrosFromJogo :: Jogo -> [Carro]
getCarrosFromJogo (Jogo _ _ cs _ _) = cs

-- | Função que converte coordenadas polares em coordenadas cartesianas.
toCartesian :: Velocidade -> Velocidade
toCartesian (m,a) = (vx,-vy)
              where
                vx = m * cos a -- componente em x do vetor velocidade
                vy = m * sin a -- componente em y do vetor Velocidade

-- | Função que converte coordenadas cartesianas em coordenadas polares.
fromCartesian :: Velocidade -> Velocidade
fromCartesian (vx,vy) = (m,a)
              where
                m = sqrt $ vx^2 + vy^2 -- modulo do vetor velocidade
                a = atan2 (-vy) vx     -- angulo do vetor velocidade

-- | Função que converte graus em radianos.
fromDegrees :: Angulo -> Angulo
fromDegrees deg = deg * pi / 180

-- | Função que converte radianos em graus.
toDegrees :: Angulo -> Angulo
toDegrees rad = rad * 180 / pi
