module Test.Engine.Refresh where

import Test.HUnit
import Test.Cases
import Engine.Refresh


testsEngineRefresh = TestLabel "Engine.Refresh" $ TestList [
    TestLabel "Teste 0" $ TestCase test0EngineRefresh,
    TestLabel "Teste 1" $ TestCase test1EngineRefresh,
    TestLabel "Teste 2" $ TestCase test2EngineRefresh,
    TestLabel "Teste 3" $ TestCase test3EngineRefresh,
    TestLabel "Teste 4" $ TestCase test4EngineRefresh,
    TestLabel "Teste 5" $ TestCase test5EngineRefresh,
    TestLabel "Teste 6" $ TestCase test6EngineRefresh
  ]

test0EngineRefresh = assertEqual "for (double 3)," 6 (double 3)
test1EngineRefresh = assertEqual "for (double 3)," 6 (double 3)
test2EngineRefresh = assertEqual "for (double 3)," 6 (double 3)
test3EngineRefresh = assertEqual "for (double 3)," 6 (double 3)
test4EngineRefresh = assertEqual "for (double 3)," 6 (double 3)
test5EngineRefresh = assertEqual "for (double 3)," 6 (double 3)
test6EngineRefresh = assertEqual "for (double 3)," 6 (double 3)