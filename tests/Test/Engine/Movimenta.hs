module Test.Engine.Movimenta where

import Test.HUnit
import Test.Cases
import Engine.Movimenta


testsEngineMovimenta = TestLabel "Engine.Movimenta" $ TestList [
    TestLabel "Teste 0" $ TestCase test0EngineMovimenta,
    TestLabel "Teste 1" $ TestCase test1EngineMovimenta,
    TestLabel "Teste 2" $ TestCase test2EngineMovimenta,
    TestLabel "Teste 3" $ TestCase test3EngineMovimenta,
    TestLabel "Teste 4" $ TestCase test4EngineMovimenta,
    TestLabel "Teste 5" $ TestCase test5EngineMovimenta,
    TestLabel "Teste 6" $ TestCase test6EngineMovimenta
  ]

test0EngineMovimenta = assertEqual "for (double 3)," 6 (double 3)
test1EngineMovimenta = assertEqual "for (double 3)," 6 (double 3)
test2EngineMovimenta = assertEqual "for (double 3)," 6 (double 3)
test3EngineMovimenta = assertEqual "for (double 3)," 6 (double 3)
test4EngineMovimenta = assertEqual "for (double 3)," 6 (double 3)
test5EngineMovimenta = assertEqual "for (double 3)," 6 (double 3)
test6EngineMovimenta = assertEqual "for (double 3)," 6 (double 3)